#ifndef AVLTree

#define AVLTREE_H
class AVLTree {

private:

	Word *root;

public:

	AVLTree() {
		root = NULL;
	}

	void setRoot(Word *root) {
		this->root = root;
	}

	Word *getRoot() {
		return root;
	}

	int height(Word *n) {
		if (n == NULL)
			return 0;
		return n->getHeight();
	}
	
	int max(int a, int b) {
		return(a > b) ? a : b;
	}

	Word *newNode(Word *word) {
		word->setLeft(NULL);
		word->setRight(NULL);
		word->setHeight(1);
		return word;
	}

	Word *rightRotate(Word *y) {
		Word *x = y->getLeft();
		Word *T2 = x->getRight();

		x->setRight(y);
		y->setLeft(T2);

		y->setHeight(max(height(y->getLeft()), height(y->getRight()) + 1));
		x->setHeight(max(height(x->getLeft()), height(x->getRight()) + 1));
		return x;
	}

	Word *leftRotate(Word *x) {
		Word *y = x->getRight();
		Word *T2 = y->getLeft();

		y->setLeft(x);
		x->setRight(T2);	

		x->setHeight(max(height(x->getLeft()), height(x->getRight()) + 1));
		y->setHeight(max(height(y->getLeft()), height(y->getRight()) + 1));
		return y;
	}

	int getBalance(Word *n) {
		if (n == NULL)
			return 0;
		return height(n->getLeft()) - height(n->getRight());
	}

	Word *insert(Word *node, Word *word) {
		if (node == NULL) 
			return newNode(word);
		if (word->getWord() < node->getWord())
			node->setLeft(insert(node->getLeft(), word));
		else if (word->getWord() > node->getWord())
			node->setRight(insert(node->getRight(), word));
		else
			return node;

		node->setHeight(max(height(node->getLeft()), height(node->getRight())) + 1);

		int balance = getBalance(node);

		if (balance > 1 && word->getWord() < node->getLeft()->getWord())
			return rightRotate(node);
		if (balance < -1 && word->getWord() > node->getRight()->getWord())
			return leftRotate(node);

		if (balance > 1 && word->getWord() > node->getLeft()->getWord()) {
			node->setLeft(leftRotate(node->getLeft()));
			return rightRotate(node);
		}

		if (balance < -1 && word->getWord() < node->getRight()->getWord()) {
			node->setRight(rightRotate(node->getRight()));
			return leftRotate(node);
		}

		return node;

	}

	void inOrder(Word *node) {
		if (node != NULL) {
			inOrder(node->getLeft());
			node->Display();
			inOrder(node->getRight());
		}
	}

	void Search(Word *node, std::string key, int &check) {
		if (node != NULL) {
			Search(node->getLeft(), key, check);
			if (node->getWord() == key) {
				std::cout << "Word " << key << " was found at " << &node << std::endl;
				node->Display();
				check = 1;
			}
			Search(node->getRight(), key, check);
		}
	}

};
#endif