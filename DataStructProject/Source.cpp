#include "Controller.h"

void Menu(std::string&);

void main() {
	Controller *controller = new Controller();
	LinkedList *list = new LinkedList();
	AVLTree *tree = new AVLTree();
	controller->ReadFile("wb1913_samp520.txt", list, tree);
	std::string choice;
	while (1) {
		system("cls");
		Menu(choice);
		while (choice != "1" && choice != "2" &&choice != "3" &&choice != "4" &&choice != "5" &&choice != "6") {
			system("cls");
			std::cout << "Invalid Input, please retry" << std::endl;
			Menu(choice);
		}
		system("cls");
		if (choice == "1") {
			controller->SortList(list);
		}
		else if (choice == "2") {
			controller->AddWord(list, tree);
		}
		else if (choice == "3") {
			controller->SearchWord(list, tree);
		}
		else if (choice == "4") {
			controller->PrintDictionary(list, tree);
		}
		else if (choice == "5") {
			controller->ValidateSentence(list, tree);
		}
		else if (choice == "6") {
			std::cout << "Exiting program..." << std::endl;
			break;
		}
		system("pause");
	}
	system("pause");
}

void Menu(std::string &choice) {
	std::cout << "Data Structure Time Complexity Test | Linked List vs AVL Tree \n \n";
	std::cout << "1) Sort List \n";
	std::cout << "2) Add Word \n";
	std::cout << "3) Search For Word \n";
	std::cout << "4) Print From Structures \n";
	std::cout << "5) Validate Sentence \n";
	std::cout << "6) Exit \n \n";
	std::cout << "Select: ";
	std::getline(std::cin, choice);
}