#ifndef Word
#define WORD_H
#include<iostream>
#include<string>
class Word {

private:
	std::string word;
	std::string definition;
	std::string pos;
	Word *next;
	Word *left;
	Word *right;
	int height;

public:

	Word() {
		word = "word";
		definition = "definition";
		pos = "part of speech";
		next = NULL;
		left = NULL;
		right = NULL; 
		height = 0;
	}

	Word(std::string word, std::string definition, std::string pos) {
		setWord(word);
		setDefinition(definition);
		setPos(pos);
	}

	Word(Word &word) {
		setWord(word.getWord());
		setDefinition(word.getDefinition());
		setPos(word.getPos());
	}

	void setWord(std::string word) {
		this->word = word;
	}

	std::string getWord() {
		return word;
	}

	void setDefinition(std::string definition) {
		this->definition = definition;
	}

	std::string getDefinition() {
		return definition;
	}

	void setPos(std::string pos) {
		this->pos = pos;
	}

	std::string getPos() {
		return pos;
	}

	void setNext(Word *next) {
		this->next = next;
	}

	Word *getNext() {
		return next;
	}

	void setLeft(Word *left) {
		this->left = left;
	}

	Word *getLeft() {
		return left;
	}

	void setRight(Word *right) {
		this->right = right;
	}

	Word *getRight() {
		return right;
	}

	void setHeight(int height) {
		this->height = height;
	}

	int getHeight() {
		return height;
	}

	const void Display() {
		std::cout << getWord() << "\t" << getDefinition() << "\t" << getPos() << "\n";
	}
};
#endif