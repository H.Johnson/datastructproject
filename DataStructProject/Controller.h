#ifndef Controller_h
#define CONTROLLER_H
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include "LinkedList.h"
#include "AVLTree.h"
#include <time.h>
#include <windows.h>
#include <limits>

std::ifstream inFile;
clock_t start;
clock_t stop;
double elapsed;

class Controller {

private:
	std::string id;

public:

	Controller() {
		id = "Basic Read/Write File Controller";
	}

	void ReadFile(std::string fileName, LinkedList *list, AVLTree *tree) {
		int x = 0;
		size_t pos = 0;
		std::string line;
		std::string token[3];
		std::string delimiter = "\t";
		inFile.open(fileName);
		if (!inFile) {
			std::cout << "Unable to open file";
			exit(1); // terminate with error
		}

		while (getline(inFile,line)) {
			while ((pos = line.find(delimiter)) != std::string::npos) {
				token[x] = line.substr(0, pos);
				line.erase(0, pos + delimiter.length());
				x++;
			} 

			token[2] = line.substr(0, pos);
			x = 0;
			Word *temp = new Word(token[0], token[1], token[2]);
			list->createnode(temp);
			tree->setRoot(tree->insert(tree->getRoot(), temp));
		}

		inFile.close();
	}
	
	void SortList(LinkedList *list) {
		char choice;
		std::cout << "Are you sure you want to sort the list? (Y/N): ";
		std::cin >> choice;
		std::cin.ignore(256, '\n');
		while (toupper(choice) != 'Y' && toupper(choice) != 'N') {
			std::cout << "Invalid input, re-enter (Y/N): ";
			std::cin >> choice;
			std::cin.ignore(256, '\n');
		}
		if (toupper(choice) == 'Y') {
			system("cls");
			std::cout << "List before sort..." << std::endl;
			system("pause");
			list->display();
			system("pause");
			PleaseWait();
			start = clock();
			list->MergeSort(list->getHead());
			stop = clock();
			elapsed = (double)(stop - start) / CLOCKS_PER_SEC;
			system("cls");
			std::cout << "List after sort..." << std::endl;
			system("pause");
			list->display();
			system("pause");
			system("cls");
			std::cout << "List took " << elapsed << " seconds to sort." << std::endl;
		}
		else if (toupper(choice) == 'N') {
			system("cls");
			std::cout << "Program will return to main menu." << std::endl;
		}
	}

	void AddWord(LinkedList *list, AVLTree *tree) {
		int search = 0;
		std::string word;
		std::string part;
		std::string definition;
		std::cout << "Enter new word: ";
		std::getline(std::cin, word);
		list->Search(word, search);
		while (search == 1) {
			system("cls");
			std::cout << "Word already exists, please try another: ";
			std::getline(std::cin, word);
			list->Search(word, search);
		}
		std::cout << "Enter part of speech: ";
		std::getline(std::cin, part);
		std::cout << "Enter defintion: ";
		std::getline(std::cin, definition);
		Word *temp = new Word(word, part, definition);
		PleaseWait();
		start = clock();
		list->createnode(temp);
		stop = clock();
		elapsed = (double)(stop - start) / CLOCKS_PER_SEC;
		system("cls");
		std::cout << "List took " << elapsed << " seconds to add new word." << std::endl;
		start = clock();
		tree->setRoot(tree->insert(tree->getRoot(), temp));
		stop = clock();
		elapsed = (double)(stop - start) / CLOCKS_PER_SEC;
		std::cout << "Tree took " << elapsed << " seconds to add new word." << std::endl;
	}

	int SearchWord(LinkedList *list, AVLTree *tree) {
		std::string key;
		int checklist = 0;
		int checktree = 0;
		std::cout << "Enter the word you wish to search for: ";
		std::getline(std::cin, key);
		system("cls");
		PleaseWait();
		start = clock();
		list->Search(key, checklist);
		stop = clock();
		elapsed = (double)(stop - start) / CLOCKS_PER_SEC;
		if (checklist == 0) {
			std::cout << "Word not found" << std::endl;
		}
		std::cout << "List took " << elapsed << " seconds to search." << std::endl << std::endl;
		start = clock();
		tree->Search(tree->getRoot(), key, checktree);
		stop = clock();
		elapsed = (double)(stop - start) / CLOCKS_PER_SEC;
		if (checktree == 0) {
			std::cout << "Word not found" << std::endl;
		}
		std::cout << "Tree took " << elapsed << " seconds to search." << std::endl << std::endl;
		return checklist;
	}

	void PrintDictionary(LinkedList *list, AVLTree *tree) {
		PleaseWait();
		start = clock();
		list->display();
		stop = clock();
		elapsed = (double)(stop - start) / CLOCKS_PER_SEC;
		system("pause");
		system("cls");
		std::cout << "List took " << elapsed << " seconds to print." << std::endl;
		start = clock();
		tree->inOrder(tree->getRoot());
		stop = clock();
		elapsed = (double)(stop - start) / CLOCKS_PER_SEC;
		system("pause");
		std::cout << "Tree took " << elapsed << " seconds to print." << std::endl;
	}

	void ValidateSentence(LinkedList *list, AVLTree *tree) {
		int x = 0, search = 0;
		double listElapsed = 0;
		double treeElapsed = 0;

		size_t pos = 0;
		std::string sentence;
		std::string token[50];
		std::string delimiter = " ";



		std::cout << "Enter sentence to be validated: ";
		std::getline(std::cin, sentence);
		while ((pos = sentence.find(delimiter)) != std::string::npos) {
			token[x] = sentence.substr(0, pos);
			Validate(token[x], search, list, tree, listElapsed, treeElapsed);
			sentence.erase(0, pos + delimiter.length());
			x++;
		}
		Validate(sentence, search, list, tree, listElapsed, treeElapsed);
		system("cls");
		std::cout << "List took " << listElapsed << " seconds to validate sentence." << std::endl;
		std::cout << "Tree took " << treeElapsed << " seconds to validate sentence." << std::endl;
	}

	void Validate(std::string token, int &search, LinkedList *list, AVLTree *tree, double &listElapsed, double &treeElapsed) {
		std::string part;
		std::string definition;
		char choice;
		start = clock();
		list->Search(token, search);
		stop = clock();
		listElapsed += (double)(stop - start) / CLOCKS_PER_SEC;
		start = clock();
		tree->Search(tree->getRoot(), token, search);
		stop = clock();
		treeElapsed += (double)(stop - start) / CLOCKS_PER_SEC;
		if (search == 0) {
			std::cout << "The word " << token << " is currently not in the dictionary, would you like to add it? (Y/N): ";
			std::cin >> choice;
			std::cin.ignore(256, '\n');
			while (toupper(choice) != 'Y' && toupper(choice) != 'N') {
				std::cout << "Invalid input, re-enter (Y/N): ";
				std::cin >> choice;
				std::cin.ignore(256, '\n');
			}
			if (toupper(choice) == 'Y') {
				std::cout << "Enter part of speech: ";
				std::getline(std::cin, part);
				std::cout << "Enter definition: ";
				std::getline(std::cin, definition);
				Word *temp = new Word(token, part, definition);
				PleaseWait();
				start = clock();
				list->createnode(temp);
				stop = clock();
				listElapsed += (double)(stop - start) / CLOCKS_PER_SEC;
				start = clock();
				tree->setRoot(tree->insert(tree->getRoot(), temp));
				stop = clock();
				treeElapsed += (double)(stop - start) / CLOCKS_PER_SEC;
			}
			else if (toupper(choice) == 'N')
				std::cout << "Word will not be added. " << std::endl;
		}
	}

	void PleaseWait() {
		system("cls");
		std::cout << "Please wait ";
		for (int x = 0; x < 3; x++) {
			std::cout << ". ";
			Sleep(500);
		}
		system("cls");
	}

};
#endif