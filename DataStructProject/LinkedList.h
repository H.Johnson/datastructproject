#ifndef LinkedList
#define LINKEDlIST_H
#include <iostream>
#include "Word.h"
class LinkedList {
private:
	Word *head, *tail;
public:
	LinkedList() {
		head = NULL;
		tail = NULL;
	}
	void setHead(Word* head) {
		this->head = head;
	}

	Word* getHead() {
		return head;
	}

	void createnode(Word *word)
	{
		Word *temp = new Word(*word);
		temp->setNext(NULL);
		if (head == NULL)
		{
			head = temp;
			tail = temp;
			temp = NULL;
		}
		else
		{
			tail->setNext(temp);
			tail = temp;
		}
	}

	void display()
	{
		Word *temp = new Word();
		temp = head;
		while (temp != NULL)
		{
			temp->Display();
			temp = temp->getNext();
		}
	}

	void insert_position(std::string key, Word *word)
	{
		Word *pre = new Word();
		Word *cur = new Word();
		Word *temp = new Word(*word);
		cur = head;
		while(key != temp->getWord()){
			pre = cur;
			cur = cur->getNext();
		}
		pre->setNext(temp);
		temp->setNext(cur);
	}

	void delete_position(std::string word)
	{
		Word *current = new Word();
		Word *previous = new Word();
		current = head;
		while (word != current->getWord()) {
			previous = current;
			current = current->getNext();
		}
		if (word == head->getWord()) {
			head = head->getNext();
		}
		else {
			previous->setNext(current->getNext());
		}
	}

	Word* MergeSort(Word *my_node)
	{
		Word *secondNode;

		if (my_node == NULL)
			return NULL;
		else if (my_node->getNext() == NULL)
			return my_node;
		else
		{
			secondNode = Split(my_node);
			return Merge(MergeSort(my_node), MergeSort(secondNode));
		}
	}

	Word* Merge(Word* firstNode, Word* secondNode)
	{
		if (firstNode == NULL) return secondNode;
		else if (secondNode == NULL) return firstNode;
		else if (firstNode->getWord() <= secondNode->getWord()) //if I reverse the sign to >=, the behavior reverses
		{
			firstNode->setNext(Merge(firstNode->getNext(), secondNode));
			return firstNode;
		}
		else
		{
			secondNode->setNext(Merge(firstNode, secondNode->getNext()));
			return secondNode;
		}
	}

	Word* Split(Word* my_node)
	{
		Word* secondNode;

		if (my_node == NULL) return NULL;
		else if (my_node->getNext() == NULL) return NULL;
		else {
			secondNode = my_node->getNext();
			my_node->setNext(secondNode->getNext());
			secondNode->setNext(Split(secondNode->getNext()));
			return secondNode;
		}
	}

	void Search(std::string key, int &check) {
		Word *temp;
		temp = head;
		if (temp == NULL) {
			std::cout << "List is empty." << std::endl;
		}
		while (temp != NULL)
		{
			if (temp->getWord() == key) {
				std::cout << "Word " << key << " was found at " << &temp << std::endl;
				temp->Display();
				check = 1;
				break;
			}
			temp = temp->getNext();
		}
	}

};
#endif